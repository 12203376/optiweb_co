var swalWithBootstrapButtons;

function onload(){
	swalWithBootstrapButtons = Swal.mixin({
	  customClass: {
	    confirmButton: "btn btn-success",
	    cancelButton: "btn btn-danger"
	  },
	  buttonsStyling: false
	});
}

function seDeconnecter(){
	swalWithBootstrapButtons.fire({
	  title: "Déconnexion ?",
	  text: "Voullez-vous vous déconnecter ?",
	  icon: "question",
	  showCancelButton: true,
	  confirmButtonText: "Oui, je me déconnecte",
	  cancelButtonText: "Non",
	  reverseButtons: true
	}).then((result) => {
	  if (result.isConfirmed) {
	   /* swalWithBootstrapButtons.fire({
	      title: "Deleted!",
	      text: "Your file has been deleted.",
	      icon: "success"
	    });*/
	  	location = "?controller=connexion&action=deconnexion";
	  } else if (
	    /* Read more about handling dismissals below */
	    result.dismiss === Swal.DismissReason.cancel
	  ) {
	    /*swalWithBootstrapButtons.fire({
	      title: "Cancelled",
	      text: "Your imaginary file is safe :)",
	      icon: "error"
	    });*/
	  }
	});
}

function confirm_spprimer_personne(idPersonne){
	swalWithBootstrapButtons.fire({
		title: "Supprimer ?",
		text: "Voullez-vous supprimer ce compte (Personne) ?",
		icon: "question",
		showCancelButton: true,
		confirmButtonText: "Oui, supprimer ce compte",
		cancelButtonText: "Annuler",
		reverseButtons: true
	  }).then((result) => {
		if (result.isConfirmed) {
			location = "?controller=annuaire&action=supprimer&id="+idPersonne;
		} else if (
		  /* Read more about handling dismissals below */
		  result.dismiss === Swal.DismissReason.cancel
		) {}
	  });
}