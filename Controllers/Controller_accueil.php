<?php 

/**
 * class Controller_accueil
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=accueil
 * 
 */
class Controller_accueil extends Controller {

    /**
     * Action accueil : redirection vers la vue view_accueil
     * Dans l'url : ...&action=accueil ...
     */
    public function action_accueil(){   
        $m=Model::getModel();
        $data=[];
        if(isset($_SESSION["id"])){
            $data=["profil" => $m->getInfoProfil($_SESSION["id"])];
        }
        $this->render("accueil", $data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'accueil'
     */
    public function action_default(){
        $this->action_accueil();
    }
}
?>