<?php 

/**
 * class Controller_annuaire
 *  
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=annuaire
 * 
 */
class Controller_annuaire extends Controller {
     /**
     * Action annuaire : redirection vers la vue view_annuaire
     * Dans l'url : ...&action=annuaire ...
     */
    public function action_annuaire(){
        $m = Model::getModel();
        
        $data = ["infos" => $m->getList(), "profil" => $m->getInfoProfil($_SESSION['id'])];

        $this->render("annuaire",$data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'annuaire'
     */
    public function action_default(){
        $this->action_annuaire();
    }

    /**
     * Action ajouter : redirection vers la vue view_ajouter
     * Dans l'url : ...&action=ajouter ...
     */
    public function action_ajouter(){
        $m = Model::getModel();
        $this->render("ajouter", ["profil" => $m->getInfoProfil($_SESSION['id'])]);
    }

    /**
     * Action ajouter_form : redirection vers la vue view_ajouter_form
     * Dans l'url : ...&action=ajouter_form ...
     */
    public function action_ajouter_form(){
        $m = Model::getModel();
        $data = ["profil" => $m->getInfoProfil($_SESSION['id']), "list" => $m->getCatDiscDpt(), "annee" => $m->getAnnee(), "semestre" => $m->getSemestre()];
        $this->render("form_ajouter", $data);
    }

    /**
     * Action ajouter : redirection vers la vue view_message
     * Dans l'url : ...&action=validation ...
     */
    public function action_validation(){
        $m=Model::getModel();
        if(preg_match("/^[0-9]+$/",$_POST["id"]) and ! $m->id_in_db($_POST["id"])){
            $infos["poste"]=$_POST["poste"];
            $infos["id"]=$_POST["id"];
            $infos["fonction"]=$_POST["poste"];
            $infos["nom"]=(!preg_match("/^ *$/",$_POST["nom"]))?$_POST["nom"]:"???";
            $infos["prenom"]=(!preg_match("/^ *$/",$_POST["prenom"]))?$_POST["prenom"]:"???";
            $infos["email"]=(!preg_match("/^ *$/",$_POST["email"]))?$_POST["email"]:"???";
            $infos["phone"]=(!preg_match("/^ *$/",$_POST["phone"]))?$_POST["phone"]:"???";
            $infos["mdp"]=(!preg_match("/^ *$/",$_POST["mdp"]))?password_hash($_POST["mdp"], PASSWORD_DEFAULT):"0";
            if($_POST["poste"]=="enseignant"){
                $infos["annee"]=$_POST["annee"];
                $infos["semestre"]=$_POST["semestre"];
                $infos["statut"]=$_POST["statut"];
                $infos["discipline"]=$_POST["discipline"];
                $infos["direction"]=$_POST["direction"];
                if(isset($_POST["departements"])){
                    $infos["departements"]=$_POST["departements"];
                }
            }
            $m->ajouterUtilisateur($infos);
            $this->render("message", ["title" => ":)","message" => "Ajout réussi !"]);
        }
        else{
            $this->action_error("Informations non valide !");
        }
    }

    public function action_supprimer(){
        if(isset($_SESSION["permission"]) and $_SESSION["permission"]=="direction"){
            $m = Model::getModel();
            $m->supprimerUtilisateur($_GET["id"]);
            $this->action_default();
        }
    }
}
?>