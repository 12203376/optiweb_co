<?php 

/**
 * class Controller_api
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  .../api/
 * Ce controlleur ne redirige par vers une vue.
 * Il répond par des données en format JSON...
 * 
 */
class Controller_api extends Controller {

    /**
     * Action api : elle répond par des données json
     * Dans l'url : /api/...
     */
    public function action_api(){
        header('Content-Type: application/json; charset=utf-8');


        $requestURI = $_SERVER['REQUEST_URI'];
        $httpMethod = $_SERVER['REQUEST_METHOD'];

        $response = "Ok, it's works ... '".$requestURI."'";

        switch ($httpMethod) {
            case "GET" : {
                $response = "It's GET Mthode : ".$response;
                break;
            }
            case "POST" : {
                $response = "It's POST Mthode : ".$response;
                break;
            }
            case "PUT" : {
                $response = "It's PUT Mthode : ".$response;
                break;
            }
            case "DELETE" : {
                $response = "It's DELETE Mthode : ".$response;
                break;
            }
        }

        $response = json_encode ($response);
        echo($response);
        die();
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'accueil'
     */
    public function action_default(){
        $this->action_api();
    }
}
?>