<?php 

/**
 * class Controller_assistance
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=accueil
 * 
 */
class Controller_assistance extends Controller {

     /**
     * Action assistance : redirection vers la vue view_assistance
     * Dans l'url : ...&action=assistance ...
     */
    public function action_assitance(){   
        $m = Model::getModel();

        $data = ["profil" => $m->getInfoProfil($_SESSION['id']),];
        

        $this->render("assistance", $data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'accueil'
     */
    public function action_default(){
        $this->action_assitance();
    }
}

?>