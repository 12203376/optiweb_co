<?php

/**
 * class Controller_consultation
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=consultation
 * 
 */
class Controller_consultation extends Controller {

    /**
     * Action consultation : redirection vers la vue view_consultation     
     * Dans l'url : ...&action=consultation ...
     */
    public function action_consultation(){
        $m=Model::getModel();
        $data["data"] = $m->getHeure($_SESSION["id"]);
        $data["profil"]=$m->getInfoProfil($_SESSION["id"]);
        $this->render("consultation_heure", $data);
    }

    /**
     * Action iut : redirection vers la vue view_consultation_iut
     * Dans l'url : ...&action=iut ...
     */
    public function action_iut(){
        $m=Model::getModel();
        $data["data"] = $m->getIUT();
        $data["profil"]=$m->getInfoProfil($_SESSION["id"]);
        $this->render("consultation_iut", $data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'consultation'
     */
    public function action_default(){
        $this->action_consultation();
    }
}
?>