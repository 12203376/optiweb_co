<?php 

/**
 * class Controller_declaration
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=declaration
 * 
 */
class Controller_declaration extends Controller {

    /**
     * Action declaration : redirection vers la vue view_delaration_form
     * Dans l'url : ...&action=declaration ...
     */
    public function action_declaration(){   
        $m = Model::getModel();
        
        $data = ["annee" => $m->getAnnee(), "semestre" => $m->getSemestre(),"departement" => $m->getDpt()];
        $data["profil"]=$m->getInfoProfil($_SESSION['id']);

        $this->render("declaration_form", $data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'declaration'
     */
    public function action_default(){
        $this->action_declaration();
    }

    /**
     * Action validation : redirection vers la vue message ou appel à l'action error
     * Dans l'url : ...&action=validation ...
     */
    public function action_validation(){
        $m=Model::getModel();
        if(preg_match("/^[0-9]+$/",$_POST["heure"])){
            $m->ajouterHeure();
            $this->render("message", ["title" => ":)","message" => "Ajout réussi !"]);
        }
        else{
            $this->action_error("Informations non valide !");
        }
    }
}
?>
