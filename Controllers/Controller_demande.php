<?php 

/**
 * class Controller_demande
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=demande
 * 
 */
class Controller_demande extends Controller {

    /**
     * Action demande : redirection vers la vue view_demande
     * Dans l'url : ...&action=demande ...
     */
    public function action_demande(){   
        $m=Model::getModel();
        $data['demande']=$m->getDemande();
        $data["profil"]=$m->getInfoProfil($_SESSION['id']);

        $this->render("demande", $data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'demande'
     */
    public function action_default(){
        $this->action_demande();
    }
}
?>