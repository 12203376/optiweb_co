<?php

/**
 * class Controller_departement
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=departement
 * 
 */
class Controller_departement extends Controller {
    /**
     * Action departement : redirection vers la vue view_departement
     * Dans l'url : ...&action=departement ...
     */
    public function action_departement(){
        $m=Model::getModel();
        $data["profil"]=$m->getInfoProfil($_SESSION["id"]);

        // tester permissions si l'utilisateur est a les droits de voir la page, sinon afficher la page 
        if($_SESSION["permission"]=="chefdedpt"){
            $data['info']=$m->getInfoDepartement($_SESSION["id"]);
            $data['nomf']=$m->getNomFormationPropose($data['info']['iddepartement']);
            $data['effectif']=$m->getEffectifDpt($data['info']['iddepartement']);
            $data['besoinh']=$m->getBesoinHeureDpt($data['info']['iddepartement']);
            $this->render("departement",$data);
            
        }
        elseif($_SESSION["permission"]=="direction"){
            $data["libelledept"] = $m->getNomDepartement();
            if(isset($_GET["id"])){
                $data["info"]=$m -> getInfoDepartement2($_GET["id"]);
                $data['nomf']=$m->getNomFormationPropose($data['info']['iddepartement']);
                $data['effectif']=$m->getEffectifDpt($data['info']['iddepartement']);
                $data['besoinh']=$m->getBesoinHeureDpt($data['info']['iddepartement']);
                $this->render("departement",$data);
            }
            $this->render("list_dpt",$data);
        }
        $this->action_error("Vous n'avez pas les permissions");
    }

    /**
     * Action demande : redirection vers la vue view_demande_form
     * Dans l'url : ...&action=demande ...
     */
    public function action_demande(){
        if($_SESSION["permission"]=="chefdedpt" || $_SESSION["permission"]=="direction") {
            $m=Model::getModel();
            $data = ["annee" => $m->getAnnee(), "semestre" => $m->getSemestre(),"departement" => $m->getDpt(), "discipline" => $m->getDiscipline(), "formation"=>$m->getFormation()];
            $data["profil"]=$m->getInfoProfil($_SESSION["id"]);
            $this->render("demande_form",$data);
        }
        $this->action_default();
    }

    /**
     * Action validation : redirection, après validation, vers la vue view_messgae ou déclencher l'action error
     * Dans l'url : ...&action=validation ...
     */
    public function action_validation() {
        $m=Model::getModel();
       if(preg_match("/^[0-9]+$/",$_POST["besoin"])){

            $m->ajouterBesoin();
            $this->render("message", ["title" => ":)","message" => "Envoi réussi !"]);
        }
        else{
            $this->action_error("Informations non valide !");
        }
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'departement'
     */
    public function action_default(){
        $this->action_departement();
    }
}
?>