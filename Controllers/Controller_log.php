<?php 

/**
 * class Controller_log
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=log
 * 
 */
class Controller_log extends Controller {

    /**
     * Action log : redirection vers la vue view_log
     * Dans l'url : ...&action=log ...
     */
    public function action_log(){   
        $m=Model::getModel();
        $data['log']=$m->getLog();
        $data["profil"]=$m->getInfoProfil($_SESSION["id"]);
        $this->render("log",$data);
    }

    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'log'
     */
    public function action_default(){
        $this->action_log();
    }
}
?>