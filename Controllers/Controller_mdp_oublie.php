<?php

/**
 * class Controller_accueil
 * 
 * Classe qui traitent totues les requettes http avec le lien :
 *  ...?controller=mdp_oublie
 * 
 */
class Controller_mdp_oublie extends Controller
{
    /**
     * Action par default : s'il l'action n'est pas précisée dans l'URL
     * ==> action 'show'
     */
    public function action_default()
    {
        $this->action_show();
    }

    /**
     * Action show : redirection vers la vue view_mdp_oublie
     * Dans l'url : ...&action=show ...
     * 
     * Action pour afficher la vue de réinitialisation du mot de passe
     */
    public function action_show()
    {
        $this->render("mdp_oublie");
    }

    /**
     * Action valid : validation de tocken et redirection vers la vue view_message
     * Dans l'url : ...&action=vlid ...
     */
    public function action_valid() {
        $token = $_GET['token'];
        $m = Model::getModel();
        $mdp_gen = $m->updatePasswordWithToken($token);
        $this->render("message", ["title" => "Réinitialisation du mot de passe", "message" => "Le nouveau mot de passe est : " . $mdp_gen]);
    }
    
    /**
     * Action submit : traitement du formulaire pour réinitialiser le mot de passe
     * Dans l'url : ...&action=submit ...
     * 
     * Action pour traiter la soumission du formulaire
     */
    public function action_submit()
    {
        if (!empty($_POST['email'])) {
            $email = htmlspecialchars($_POST['email']);
            // Générer le token et le hash
            $token = bin2hex(random_bytes(16));
            $token_hash = hash("sha256", $token);
            $expires_at = date("Y-m-d H:i:s", time() + 60 * 30); // 30 minutes expiration
            
            // Appeler le modèle pour enregistrer le token
            $m = Model::getModel();
            if ($m->saveResetToken($email, $token_hash, $expires_at)) {
                // Logique pour gérer l'envoi de l'email de réinitialisation
                // Vous pouvez utiliser une bibliothèque comme PHPMailer pour envoyer l'email
                // Par exemple :
                // $this->sendResetEmail($email, $token);

                /*
                $port = $_SERVER['SERVER_PORT'];
                $host = $_SERVER['SERVER_NAME'];
                $protocol = "http";
                if(strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https') {
                    return "https";
                }

                $url = $protocol."://".$host.":".$port."/";
                */

                $url = $_SERVER['HTTP_REFERER'];
                $parts = explode("?", $url);
                $verificationLink = $parts[0]."?controller=mdp_oublie&action=valid&token=" . urlencode($token);

                EmailSender::sendVerificationEmail($email, 'Vérification de l\'adresse e-mail', 'Cliquez sur le lien suivant pour vérifier votre adresse e-mail: ' . $verificationLink);
                $this->render("message", ["title" => "Réinitialisation du mot de passe", "message" => "Un email a été envoyé à $email pour réinitialiser votre mot de passe."]);
            
            } else {
                $this->action_error("Impossible de générer un token de réinitialisation.");
            }
        } else {
            $this->action_error("Email invalide !");
        }
    }

    // Méthode pour envoyer l'email de réinitialisation (optionnelle)
    // private function sendResetEmail($email, $token) {
    //     // Implémentez l'envoi de l'email ici
    // }
}
?>
