# OptiWeb_Co

## Bienvenue chez OptiWebCo

Nous analysons le code, veillons aux failles de sécurité, optimisons et améliorons le site.

## L'équipe d'OptiWeb

L'équipe d'OptiWebCo est constituée de :
Boulangeot Nathan, Krim Yanis, Billo Mamadou, Yuker Deniz.

### Le cadre du projet

Ce projet est réalisé dans le cadre de la SAÉ S4, groupe Rubis.

### Rappel Connexions

### L'ancienne méthode
L'identifiant 123 avec le mot de passe Topaze est un administrateur et a tous les droits.
L'identifiant 14 avec le mot de passe Topaze est un enseignant et ne possède pas tout les droits.

### Modification de la connexion 
Il faut introduire l'email ou non l'identifiant numérique,
Par exemple : jean.dupont@univ-paris13.com /  alexandre.levesque@univ-paris13.com ou luc.blanchard@univ-paris13.com ....

***

### Git
```
git remote add origin https://gitlab.sorbonne-paris-nord.fr/12203376/optiweb_co.git
git branch -M main
git push -uf origin main
```

***

### Documentation doxygen
Pour accéder à la documentation doxygen, ouvrir le fichier ./doc-doxygen/html/index.html

### Base de données
Pour que la page "Mot de passe oublié" puisse fonctionner correctement, vous devez modifier la base de données en ajoutant les éléments suivants :

```sql
ALTER TABLE personne
  ADD COLUMN reset_token_hash VARCHAR(200) DEFAULT NULL,
  ADD COLUMN reset_token_expires_at TIMESTAMP DEFAULT NULL,
  ADD UNIQUE (reset_token_hash);
```

