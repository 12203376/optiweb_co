<?php require("Views/partials/view_head.php"); ?>
<?php error_reporting(0);
ini_set('display_errors', 0);
?>

<body class="base" onload="onload()">
		<nav class="navbar navbar-expand-lg bg-or-primaire">
			<div class="container-fluid">
				<a class="navbar_logo" href="?controller=accueil&action=accueil">
					<img src="Content/img/LOGOTYPE_BLANC.png" alt="Logo">
				</a>

				<span class="profil"><?=e(strtoupper($profil["nom"]))." ".e($profil["prenom"])." (".$profil["role"].")"?></span>

			  	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="menu_h">
						<img src="Content/img/icons8-hamburger-menu.png" alt="Logo">
					</span>
			  	</button>

			  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav ms-auto">
				  		<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="?controller=accueil&action=accueil">Accueil</a>
				  		</li>

				  		<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Profil</a>
							<ul class="dropdown-menu">
					  			<li>
									<a class="dropdown-item" href="?controller=profil&action=profil">Profil personnel</a>
								</li>
					  			<li>
									<a class="dropdown-item" href="?controller=annuaire&action=annuaire">Annuaire</a>
								</li>
							</ul>
				  		</li>

					<?php if($_SESSION["permission"]!="secretaire" and $_SESSION["permission"]!="personne"): ?>
				  		<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Heures</a>
							<ul class="dropdown-menu">
					  			<li>
									<a class="dropdown-item" href="?controller=consultation&action=consultation">Consultation</a>
								</li>
					  			<li>
									<a class="dropdown-item" href="?controller=declaration&action=declaration">Déclaration</a>
								</li>
							</ul>
				  		</li>
					<?php endif ?>

					<?php if ($_SESSION['permission'] == "chefdedpt" or $_SESSION['permission'] == "direction") :?>
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="?controller=demande&action=demande">Demandes</a>
				  		</li>
					<?php endif ?>
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="?controller=assistance&action=assistance">Assistance</a>
				  		</li>
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="#" onclick="seDeconnecter()">Déconnexion</a>
				  		</li>
					</ul>
			  	</div>
			</div>
		</nav>
