<?php
    /**
     * Titre de la page ...
     */
    $title = "Titre de la page actuelle";

    $title= "Connexion";
    require "Views/partials/view_head.php";
?>

<body class="body_co">
    <!--container-fluid permet d'obtenir un affichage responsive-->
    <div id="cont_co" class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6 col-md-8 col-sm-10 col-12 form_co">
                <p class="co"><strong>Connexion</strong></p>

                <form action="?controller=connexion&action=check" method="post">
                    <div class="form-group">
                        <label for="email">
                            <img class='icone_co' src='Content/img/icons8-user-100.png'/>
                            Identifiant (Email)
                        </label>
                        <input class="form-control" type="email" name="email" id="email" required/>
                    </div>
                    
                    <div class="form-group">
                        <label for="mdp">
                            <img class='icone_co' src='Content/img/icons8-lock-100.png' />
                            Mot de passe
                        </label>
                        <input class="form-control" type="password" name="mdp" id="mdp" required/>
                    </div>

                    <?php if(isset($message)): ?>
                        <p class='psswrd'> <?= $message ?> </p>
                    <?php endif ?>
                    <p>
                        <a href="?controller=mdp_oublie&action=show">Mot de passe oublié?</a>
                    </p>
                    <label>
                        <input class="bouton" type="submit" value="> Se connecter" />
                    </label>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

