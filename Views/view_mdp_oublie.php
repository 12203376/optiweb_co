<?php
$title = "Réinitialiser le mot de passe";
require "./Views/partials/view_head.php";
?>

<body class="body_co">
    <!--container-fluid permet d'obtenir un affichage responsive-->
    <div id="cont_co" class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6 col-md-8 col-sm-10 col-12 form_co">
                <p class="co"><strong>Réinitialiser le mot de passe</strong></p>

                <form action="?controller=mdp_oublie&action=submit" method="post">
                    <div class="form-group">
                        <label for="email">
                            <img class='icone_co' src='/Content/img/icons8-email-100.png'/>
                            Adresse e-mail
                        </label>
                        <input class="form-control" type="email" name="email" id="email" required/>
                    </div>

                    <label>
                        <input class="bouton" type="submit" value="> Réinitialiser le mot de passe" />
                    </label>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
