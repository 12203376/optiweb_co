/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "OptiWebCo", "index.html", [
    [ "OptiWeb_Co", "index.html", [
      [ "Bienvenue chez OptiWebCo", "index.html#autotoc_md1", null ],
      [ "L'équipe d'OptiWeb", "index.html#autotoc_md2", [
        [ "Le cadre du projet", "index.html#autotoc_md3", null ],
        [ "Rappel Connexions", "index.html#autotoc_md4", null ],
        [ "L'ancienne méthode", "index.html#autotoc_md5", null ],
        [ "Modification de la connexion", "index.html#autotoc_md6", null ],
        [ "Git", "index.html#autotoc_md7", null ],
        [ "Documentation doxygen", "index.html#autotoc_md8", null ],
        [ "Base de données", "index.html#autotoc_md9", null ]
      ] ]
    ] ],
    [ "README", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html", [
      [ "PHPMailer – A full-featured email creation and transfer class for PHP", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md10", [
        [ "Features", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md11", null ],
        [ "Why you might need it", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md12", null ],
        [ "License", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md13", null ],
        [ "Installation & loading", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md14", null ],
        [ "Legacy versions", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md15", [
          [ "Upgrading from 5.2", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md16", null ],
          [ "Minimal installation", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md17", null ]
        ] ],
        [ "A Simple Example", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md18", null ],
        [ "Documentation", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md19", null ],
        [ "Tests", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md20", null ],
        [ "Security", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md21", null ],
        [ "Contributing", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md22", null ],
        [ "Sponsorship", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md23", null ],
        [ "PHPMailer For Enterprise", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md24", null ],
        [ "Changelog", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md25", null ],
        [ "History", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md26", [
          [ "What's changed since moving from SourceForge?", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__96be216d6353f1cb6802171651f7ab09.html#autotoc_md27", null ]
        ] ]
      ] ]
    ] ],
    [ "Security notices relating to PHPMailer", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__ac61ba89dee9beac1f47a350998909e0.html", null ],
    [ "Upgrading from PHPMailer 5.2 to 6.0", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html", [
      [ "PHP Version", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md30", null ],
      [ "Loading PHPMailer", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md31", null ],
      [ "Namespace", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md32", null ],
      [ "Namespaced exceptions", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md33", null ],
      [ "OAuth2 Support", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md34", null ],
      [ "Extras", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md35", null ],
      [ "Other upgrade changes", "md__home_redouane__desktop__english_00_-__a_i_m_a_d__6__i_y_a_d_e_learn__yanis__s4__s_a_e_4_80__654226fd5c4cf5097a6eb140fe070036.html#autotoc_md36", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", "globals_vars" ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"_controller_8php.html",
"class_p_h_p_mailer_1_1_p_h_p_mailer_1_1_p_h_p_mailer.html#a974888b4d036404356394cf76035174d",
"index.html#autotoc_md3",
"phpmailer_8lang-cs_8php.html#a53b73fedc58a3e9084bd6de0f3f1e4e1",
"phpmailer_8lang-he_8php.html#a4ce303a3e848c4e95a193d1db9719998",
"phpmailer_8lang-mg_8php.html#a74fde728a807ad6886041cb2be6dadc5",
"phpmailer_8lang-si_8php.html#ae9940d910460e388276ce1a4c02081f7",
"view__consultation__heure_8php.html#ada57e7bb7c152edad18fe2f166188691"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';