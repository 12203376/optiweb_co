<?php

/**
 * Ceci est le fichier qui sert de point d'entrée au site : Contrôleur frontal principal (Main-Controller)
 * 
 */

session_start();

// Pour avoir la fonction e()
require_once "Utils/functions.php";
// Inclusion de EmailSender
require_once "Utils/EmailSender.php";
// Inclusion du modèle
require_once "Models/Model.php";
// Inclusion de la classe Controller
require_once "Controllers/Controller.php";

/**
 * Tableau qui représente la liste des contrôleurs disponibles.
 */
$controllers = ["controlleur_1", "controlleur_2", "..." , "controlleur_n"];

/**
 * Nom du contrôleur par défaut.
 */
$controller_default = "<nom_controlleur>";

/**
 * Nom du contrôleur sélectionné.
 */
$nom_controller = "<nom_controlleur_sélectionné>";



$controllers = ["accueil", "connexion", "profil", "annuaire", "assistance", "consultation", "departement", "declaration", "demande", "log", "mdp_oublie"];


$controller_default = "accueil";

$requestURI = $_SERVER['REQUEST_URI'];
if (strpos($requestURI, "/api/") !== false){
    $controller_default = "api";
}

// On teste si le paramètre controller existe et correspond à un contrôleur de la liste $controllers
if ( isset($_GET['controller']) && in_array($_GET['controller'], $controllers) )
{
    $nom_controller = $_GET['controller'];
}
else
{
    $nom_controller = $controller_default;
}

/**
 * Nom de la classe du contrôleur.
 */
$nom_classe = 'Controller_' . $nom_controller;

/**
 * Nom du fichier contenant la définition du contrôleur.
 */
$nom_fichier = 'Controllers/' . $nom_classe . '.php';

/* Si le fichier existe et est accessible en lecture */
if ( is_readable($nom_fichier) ) 
{
    // On l'inclut et on instancie un objet de cette classe
    include_once $nom_fichier;

    /**
     * Instancie la classe sélectionnée.
     */
    new $nom_classe();
}
else
{
    /**
     * Sinon, affiche un message d'erreur.
     */
    die("Error 404: not found!");
}

?>
